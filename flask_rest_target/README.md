
# Rest Target Documentation
----------

This is the primary target for examples provided in the GitLab API Security
documentation and this SDK.

The rest target is a working example of a REST API backed by a SQLite database.
The example target contains one or more SQL injection vulnerabilities that can be 
identified by fuzzing the API.

For additional information regarding this example please see the
GitLab API Security documentation.

This test target is built with the following stack:

- Python
- Flask
- Flask RESTful
- SQLite


## Execute application

### Local

The application requires you to have `python` and `pip` installed. It is not required but recommended install and use `virtualenv` to prevent packages conflicts.
Check [How to install](INSTALL.md) to get more details in local execution.

### Docker

To run the docker image assume you are in the root repo directory, you can use the following commands:

```bash
# build docker image
docker build -t image-target-flask -f "./flask_rest_target/Dockerfile"  "./flask_rest_target"
# executes container from docker image
docker run -it --rm --name container-target-flask -p 7777:7777 -p 7778:7778 -p 514:514 -e SYSLOG_TARGET=localhost image-target-flask
```

## End-To-End Tests

`Flask Target` is used by End-To-End (E2E) applications, check out [How to create E2E Tests](E2E_TESTS.md) to create more E2E tests.