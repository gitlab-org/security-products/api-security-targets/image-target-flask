## Installation
----------

This target requires some Python modules to run.

```shell
pip install -r requirements.txt
```
 
## Running
----------

Target runs with Python 2.7.

```shell
python rest_target.py
```

For SSL:

```shell
python rest_ssl_target.py
```

