## Creating an E2E test

Please follow the next steps to set up your E2E test and your source code project. Once everything has been set up, the following validations will be automatically performed:

1. Verify job completes with success
1. Verify report vulnerability count
1. Verify all report assets can be downloaded

### Set up E2E Project

1. Create a new project for your E2E test. The project can be created from scratch or take advantage of the [Project import/export](https://docs.gitlab.com/ee/user/project/settings/import_export.html) and make the proper adjustments. 
1. Add or edit your `.gitlab-ci.yml` file, make sure it uses the proper base template:
    1. Use base [`template-dastapi.yml`](https://gitlab.com/gitlab-org/security-products/tests/api-fuzzing-e2e/e2e-image/-/raw/master/template-dastapi.yml) template for DAST API E2E projects
    1. Use base [`template-apifuzzing.yml`](https://gitlab.com/gitlab-org/security-products/tests/api-fuzzing-e2e/e2e-image/-/raw/master/template-apifuzzing.yml) template for API Fuzzing E2E projects
1. Edit your `.gitlab-ci.yml` file, make sure to add the environmental variables based on your test needs.
    1. `TARGET_URL` [Optional] Target image to start as service with alias of target. Defaults to [`image-target-flask`](https://gitlab.com/gitlab-org/security-products/api-security-targets/image-target-flask). Check [api-security-targets](https://gitlab.com/gitlab-org/security-products/api-security-targets) group to see more available targets.
    1. For specific DAST API variables see [DAST API - Available CI/CD variables](https://docs.gitlab.com/ee/user/application_security/dast_api/#available-cicd-variables)
    1. For specific API Fuzzing variables see [API Fuzzing - Available CI/CD variables](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/#available-cicd-variables)

E2E project's `.gitlab-ci.yml` file example for _DAST API_

```yaml
#
# Include base template from this repository
include:
    - https://gitlab.com/gitlab-org/security-products/tests/api-fuzzing-e2e/e2e-image/-/raw/master/template-dastapi.yml
           
variables:
    #
    # Set variables to configure API Security
    DAST_API_HAR: test_rest_target.har
    DAST_API_OVERRIDES_ENV: '{"headers":{"Authorization":"Token b5638ae7-6e77-4585-b035-7d9de2e3f6b3"}}'
# end
```

E2E project's `.gitlab-ci.yml` file example for _API Fuzzing_

```yaml
#
# Include base template from this repository
include:
    - https://gitlab.com/gitlab-org/security-products/tests/api-fuzzing-e2e/e2e-image/-/raw/master/template-apifuzzing.yml
           
variables:
    #
    # Set variables to configure API Security
    FUZZAPI_OPENAPI: test_openapi.v2.0.json
    FUZZAPI_OVERRIDES_ENV: '{"headers":{"Authorization":"Token b5638ae7-6e77-4585-b035-7d9de2e3f6b3"}}'
# end
```

### Set up GitLab's API Fuzzing project

Once the E2E project is ready. It needs to be invoked from [`.gitlab-ci.yml`](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/blob/master/.gitlab-ci.yml) used in [GitLab's API Fuzzing project](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/).

In order to invoke the project, you will have to add 3 new jobs (one per environment). Each job name follows this format `<environment-suffix>_<escaped-e2e-test-name>`. 

- Replace `<environment-suffix>` with the proper suffix:
  - Use the suffix `e2e_p` for `Production` environment.
  - Use the suffix `e2e_s` for `Stage` environment.
  - Use the suffix `e2e_sm` for `Stage Manual` environment.
- Replace `<escaped-e2e-test-name>` with the project name. Make sure project name uses supported characters. For instance `postman-v2.1-vars-tls` changes to `postman_v2.1_vars_tls`.

As for example if the project name is `dast-graphql-har`  and we are adding the job for `Production` environment, then the job name will be  `e2e_p_dast_graphql_har`.

The selected suffix will be also used in two more items:

- To specify the base job when using `extends` keyword. For instance, `extends: .e2e_p_base` indicates that our new job extends the `Production` E2E job.
- To specify the inherited variables in the job. For example: `<< : *e2e_p_base_variables` is used to include variables defined in `Production` environment.

Lets assume following snippet is a job template:

```yaml
e2e_s_dast_graphql_har:
    extends: .e2e_s_base
    variables:
      << : *e2e_s_base_variables
      EXPECT_REPORT_COUNT_MIN: 4
      EXPECT_REPORT_COUNT_MAX: 5
    trigger:
        project: gitlab-org/security-products/tests/api-fuzzing-e2e/dast-graphql-har
        strategy: depend
```

Thus, we would like create the 3 jobs for the project `postman-v2.1-vars-tls`, thus suffix will be  `e2e_p` for `Production`, `e2e_s` for `Stage`, and  `e2e_sm` for `Stage Manual`. The following change samples are for `Production`, but they should be done for each environment:
- **Update environment prefix.** 
    - Update job name prefix. E.g. `e2e_s_dast_graphql_har` changes to `e2e_p_dast_graphql_har`
    - Update base job prefix. E.g. `e2e_s_base` changes to `e2e_p_base`
    - Update imported variable prefix. E.g. `e2e_s_base_variables` changes `e2e_p_base_variables`
- **Update project reference.** 
    - Update job name prefix. E.g. `e2e_p_dast_graphql_har` in `e2e_p_postman_v2.1_vars_tls`
    - Update project url. E.g. `gitlab-org/security-products/tests/api-fuzzing-e2e/dast-graphql-har` to `gitlab-org/security-products/tests/api-fuzzing-e2e/postman-v2.1-vars-tls`.
- **Update variables.**
    - `EXPECT_REPORT_COUNT_MIN` -- [Required] Minimum (inclusive) expected vulnerability count. No default.
    - `EXPECT_REPORT_COUNT_MAX` -- [Required] Maximum (inclusive) expected vulnerability count. No default.

After applying the above changes to our sample template, we should get a job for `postman-v2.1-vars-tls` in `Production` environment. It should look like: 

```yaml
e2e_p_postman_v2.1_vars_tls:
    extends: .e2e_p_base
    variables:
        << : *e2e_p_base_variables
        EXPECT_REPORT_COUNT_MIN: 4
        EXPECT_REPORT_COUNT_MAX: 6
    trigger:
        project: gitlab-org/security-products/tests/api-fuzzing-e2e/postman-v2.1-vars-tls
        strategy: depend
```

Make sure to create a job per environment: `Stage`, `Stage-Manual`, and `Production`. All jobs in the `.gitlab-ci.yml` are grouped by environment, make sure to add each job in their proper section.