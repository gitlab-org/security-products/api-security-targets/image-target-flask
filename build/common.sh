#!/bin/bash

get_or_build_image(){

  local name=$1
  local docker_file=$2
  local dir=$3

  if [[ ! -e "${docker_file}" ]]; then
      echo "Dockerfile could not be found: ${docker_file}"
      exit 1
  fi

  # See if we have a cached version
  local image_name=${name}_`md5sum ${docker_file} | sed -e "s/^\\\\\\//" | cut -c -32`

  if [[ "$CI_JOB_TOKEN" != "" ]]; then
      docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

      # Pull image if it exists
      set +e
      docker pull ${CI_REGISTRY_IMAGE}:${image_name}
      set -e

      # Check and see if it pulled okay
      if [[ "$(docker images -q ${CI_REGISTRY_IMAGE}:${image_name} 2> /dev/null)" == "" ]]; then
          echo "${image_name} not found on host, building and pushing"
          docker build \
              -t ${name} \
              -t ${image_name} \
              -t ${CI_REGISTRY_IMAGE}:${image_name} \
              -f ${docker_file} \
              $dir

          docker push ${CI_REGISTRY_IMAGE}:${image_name}
      else
          echo "${image_name} found in repo, tagging"
          docker tag ${CI_REGISTRY_IMAGE}:${image_name} ${image_name}
          docker tag ${image_name} ${name}
      fi
  else
      # local build
      if [[ "$(docker images -q ${image_name} 2> /dev/null)" == "" ]]; then
          echo "${image_name} not found on host, building and tagging"
          docker build \
              -t ${name} \
              -t ${image_name} \
              -f ${docker_file} \
              $dir
      else
          echo "${image_name} found in repo, tagging"
          docker tag ${image_name} ${name}
      fi
  fi

}
